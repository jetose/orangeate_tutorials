package com.orangeate.tutorials.keycloak;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by Administrator on 2019/10/9.
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.orangeate.tutorials.keycloak.configuration"})
public class KeycloakHelloWorldApplication {

    public static void main(String[] args) {
        SpringApplication.run(KeycloakHelloWorldApplication.class, args);
    }

}
