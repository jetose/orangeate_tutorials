package com.orangeate.tutorials.keycloak.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Administrator on 2019/10/9.
 */
@RestController
public class HelloWorldController {

    @GetMapping(path = "/")
    public String index() {
        return "external";
    }

    @GetMapping(path = "/customers")
    public String customers(Model model) {
//        addCustomers();
//        model.addAttribute("customers", customerDAO.findAll());
        return "customers";
    }

}
